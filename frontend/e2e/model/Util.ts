import {browser, ElementFinder} from 'protractor';
import {promise as wdpromise} from 'selenium-webdriver';
import * as _ from 'underscore';

export class Log {

    public static info(msg: string | any): void {
        browser.controlFlow().execute(() => {
            // tslint:disable-next-line no-console
            console.log(msg);
        });
    }
}

export class When {
    private condValue: any;

    constructor(condition: () => boolean | boolean) {
        this.condValue = _.isFunction(condition) ? condition() : condition;
    }

    public then(expr): When {
        this.fnThen = _.isFunction(expr) ? expr : () => expr;
        return this;
    }

    public else(expr): When {
        this.fnElse = _.isFunction(expr) ? expr : () => expr;
        return this;
    }

    public return(result?): any {
        const evaluated = this.eval();
        return _.isUndefined(result) ? evaluated : result;
    }

    private fnThen: () => any = () => undefined;
    private fnElse: () => any = () => undefined;

    private eval(): any {
        return this.condValue ? this.fnThen() : this.fnElse();
    }
}

export class Condition {

    public static when(condition: () => boolean | boolean): When {
        return new When(condition);
    }
}

export class TestHelper {

    public static disableAnimations(): wdpromise.Promise<any> {
        const css = '* {' +
            '-webkit-transition: none !important;' +
            '-moz-transition: none !important;' +
            '-o-transition: none !important;' +
            '-ms-transition: none !important;' +
            'transition: none !important;' +
            'animation: none !important;' +
            '-webkit-animation: none !important' +
            '-moz-animation: none !important;' +
            '-o-animation: none !important;' +
            '-ms-animation: none !important;' +
            '}';
        const script = '$(\'<style type=\"text/css\">' + css + '</style>\').appendTo($(\'body\'));';
        return browser
            .executeScript(script)
            .then(() => browser.sleep(100));
    }

    public static hasClass(element: ElementFinder, clazz: string): wdpromise.Promise<boolean> {
        return element.getAttribute('class').then((classes) => classes.split(' ').indexOf(clazz) !== -1);
    }

    public static randomInt(upperBound: number, lowerBound?: number): number {
        const offset = lowerBound || 0;
        return offset + Math.floor(Math.random() * (upperBound - offset));
    }

    public static isoDateTime(): string {
        return new Date().toISOString().replace('T', ' ');
    }
}

export class XPathUtil {

    /**
     * Returns an XPath condition that acts like the CSS selector ':has' for child nodes.
     *
     * @param {string} element
     * @param {string} condition
     */
    public static hasChild(element: string, condition?: string) {
        return 'child::' + element + (condition ? ('[' + condition + ']') : '');
    }

    public static hasClass(clazz: string) {
        return XPathUtil.attrContainsToken('class', clazz);
    }

    /**
     * Returns an XPath condition that acts like the CSS attribute word selector "attribute~=word".
     *
     * @param {string} attribute The attribute to match against
     * @param {string} token The token word that has to be contained in the attribute value
     * @returns {string} The XPath expression
     */
    public static attrContainsToken(attribute: string, token: string) {
        // XPath function 'contains-token()' is XPath 3.1 so we can't use it here yet
        return 'contains(concat(" ", normalize-space(@' + attribute + '), " "), " ' + token + ' ")';
    }
}
