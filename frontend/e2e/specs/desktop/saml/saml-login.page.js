(function () {
  'use strict';

  var loginPage = require('../../login.page.js');
  var testhelper = require('../../../testhelper');

  var api = {
    // TODO handle in protractor conf
    // eslint-disable-next-line
    idpBaseUrl: process.env.bamboo_COYO_E2E_SAML_IDP_BASEURL || process.env.COYO_E2E_SAML_IDP_BASEURL
    || 'http://localhost:8180',
    trigger: trigger,
    username: $('#username'),
    password: $('#password'),
    loginButton: $('#regularsubmit .btn'),
    login: login,
    idpLogout: idpLogout
  };

  function trigger(name) {
    loginPage.get();
    loginPage.selectDefaultBackend();
    var authProvicersSelector = by.repeater('config in $ctrl.authenticationProviderConfigs');
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(element(authProvicersSelector)));
    var button = element.all(authProvicersSelector)
        .filter(testhelper.filterElementTextContains(name))
        .first();
    browser.ignoreSynchronization = true;
    button.click();

    browser.sleep(1000);
    api.username.isPresent().then(function (present) {
      if (!present) {
        browser.sleep(5000);
        browser.ignoreSynchronization = false;
        loginPage.get();
        loginPage.logout();
        loginPage.get();
        trigger();
      }
    });
    browser.ignoreSynchronization = false;
  }

  function login(username, password) {
    browser.ignoreSynchronization = true;
    api.username.sendKeys(username);
    api.password.sendKeys(password);
    api.loginButton.click();
    browser.ignoreSynchronization = false;
    browser.sleep(3000);
    browser.getCurrentUrl().then(function (actualUrl) {
      if (actualUrl.endsWith('/search')) {
        browser.get('/home');
      }
    });
    testhelper.cancelTour();
  }

  function idpLogout() {
    browser.ignoreSynchronization = true;
    browser.get(api.idpBaseUrl + '/simplesaml/module.php/core/authenticate.php?as=example-userpass&logout');
    browser.ignoreSynchronization = false;
    browser.get('/search');
    testhelper.disableAnimations();
  }

  module.exports = api;

})();
