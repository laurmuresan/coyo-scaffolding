(function () {
  'use strict';

  function WorkspaceDetails() {
    var api = this;

    api.title = $('.title');

    api.navigation = {
      apps: element.all(by.repeater('app in ::$ctrl.getAppsByGroup(appGroup)')),
      openAppSettings: function (elem) {
        elem.$('.btn-settings').click();
      }
    };

    api.options = {
      addApp: $('.content-sidebar a[ng-click="$ctrl.addApp($ctrl.workspace, $ctrl.apps)"]'),
      settings: $('.content-sidebar a[ui-sref="main.workspace.show.settings"]')
    };

    api.settings = {
      deleteButton: $('a[ng-click="$ctrl.delete()"]')
    };
  }

  module.exports = WorkspaceDetails;
})();
