(function () {
  'use strict';

  var multiLanguage = require('./admin.multi-language.page');

  module.exports = {
    get: function () {
      multiLanguage.get();
      multiLanguage.navigationBar.translationsButton.click();
    },
    form: {
      search: $('.admin-translations coyo-filterbox'),
      languageFilter: $('.admin-translations coyo-filter'),
      adaptedTranslationsCheckbox: $('.admin-translations .checkbox'),
      table: $('.admin-translations table')
    }
  };

})();
