(function () {
  'use strict';

  var BtnLikes = require('./btn-likes.page');

  function Comment(commentElement) {
    var api = this;
    api.element = commentElement;
    api.author = api.element.$('.comment-author');
    api.message = api.element.$('.comment-message');
    api.likeBtn = BtnLikes.create(api.element);
  }

  Comment.getAllElements = function (context) {
    return context.$$('.comment');
  };

  Comment.create = function (context, index) {
    var commentElement = Comment.getAllElements(context).get(index);
    return new Comment(commentElement);
  };

  module.exports = Comment;

})();
