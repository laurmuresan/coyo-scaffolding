(function () {
  'use strict';

  var extend = require('util')._extend;
  var Select = require('../../../select.page');

  function WikiArticleWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      wikiArticle: new Select($('coyo-select-wiki-article-widget'))
    };

    api.renderedWidget = {
      articleSender: element(by.css('.article-sender')),
      articleTitle: element(by.css('.article-title'))
    };
  }

  module.exports = WikiArticleWidget;
})();
