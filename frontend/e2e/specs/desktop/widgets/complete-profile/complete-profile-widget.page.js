(function () {
  'use strict';

  var extend = require('util')._extend;

  function CompleteProfileWidget(widget) {
    var api = extend(this, widget);

    api.renderedWidget = {
      profileItem: function (label) {
        return element(by.cssContainingText('[ng-class="::{\'completed\': item.checked, \'bold\': !item.checked}"]',
            label)).$('i.zmdi');
      },
      isNotChecked: function (label) {
        return api.renderedWidget.profileItem(label).getAttribute('class').then(function (classes) {
          return classes.split(' ').indexOf('zmdi-close') !== -1;
        });
      },
      links: element.all(by.css('[ng-class="::{\'completed\': item.checked, \'bold\': !item.checked}"]'))
    };

    api.filterColleaguesList = $('span[translate="MODULE.COLLEAGUES.FILTER.ALL"]');
    api.followUser = element.all(by.css('[uib-tooltip="Click to Follow"]')).first();
    api.followPage = $$('.page-subscribe.btn-default');

  }
  module.exports = CompleteProfileWidget;
})();
