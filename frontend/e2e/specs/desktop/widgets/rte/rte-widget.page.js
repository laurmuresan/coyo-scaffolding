(function () {
  'use strict';

  var extend = require('util')._extend;

  function RteWidget(widget) {
    extend(this, widget);
  }

  module.exports = RteWidget;
})();
