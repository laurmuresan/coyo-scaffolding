// tslint:disable-next-line no-namespace
declare namespace jasmine {
    // tslint:disable-next-line interface-name
    interface Matchers<T> {
        toHaveText(text: string): boolean;

        toContainText(text: string): boolean;

        toHaveClass(text: string): boolean;

        toBeVisible(): boolean;
    }
}
