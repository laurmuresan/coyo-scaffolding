'use strict';

var path = require('path');
var conf = require('./gulp/conf');

var _ = require('lodash');
var wiredep = require('wiredep');

var pathSrcHtml = [
  path.join(conf.paths.src_main, '/**/*.html')
];

function listFiles() {
  var wiredepOptions = _.extend({}, conf.wiredep, {
    dependencies: true,
    devDependencies: true
  });

  return wiredep(wiredepOptions).js
    .concat([
      path.join(conf.paths.src_main, '/config.js'),
      path.join(conf.paths.src_main, '/app/**/*.js'),
      path.join(conf.paths.test, '/global-config.js'),
      path.join(conf.paths.test, '/**/*.spec.js')
    ])
    .concat(pathSrcHtml);
}

module.exports = function(config) {

  var configuration = {
    files: listFiles(),

    singleRun: true,

    autoWatch: false,

    browserNoActivityTimeout: 60000,

    captureTimeout: 60000,

    ngHtml2JsPreprocessor: {
      stripPrefix: conf.paths.src_main + '/',
      moduleName: 'coyo.templates'
    },

    logLevel: 'WARN',

    frameworks: ['jasmine', 'jasmine-matchers', 'angular-filesort'],

    angularFilesort: {
      whitelist: [path.join(conf.paths.src_main, '/**/!(*.html|*.spec|*.mock).js')]
    },

    browsers : ['PhantomJS'],

    plugins : [
      'karma-phantomjs-launcher',
      'karma-angular-filesort',
      'karma-coverage',
      'karma-jasmine',
      'karma-jasmine-matchers',
      'karma-junit-reporter',
      'karma-ng-html2js-preprocessor'
    ],

    reporters: ['progress', 'junit'],

    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },

    junitReporter: {
      outputDir: 'test-results/',
      suite: 'com.mindsmash.coyo.frontend',
      useBrowserName: true
    }
  };

  // This is the default preprocessors configuration for a usage with Karma cli
  // The coverage preprocessor is added in gulp/unit-test.js only for single tests
  // It was not possible to do it there because karma doesn't let us now if we are
  // running a single test or not
  configuration.preprocessors = {};
  pathSrcHtml.forEach(function(path) {
    configuration.preprocessors[path] = ['ng-html2js'];
  });

  // This block is needed to execute Chrome on Travis
  // If you ever plan to use Chrome and Travis, you can keep it
  // If not, you can safely remove it
  // https://github.com/karma-runner/karma/issues/1144#issuecomment-53633076
  if(configuration.browsers[0] === 'Chrome' && process.env.TRAVIS) {
    configuration.customLaunchers = {
      'chrome-travis-ci': {
        base: 'Chrome',
        flags: ['--no-sandbox']
      }
    };
    configuration.browsers = ['chrome-travis-ci'];
  }

  config.set(configuration);
};
