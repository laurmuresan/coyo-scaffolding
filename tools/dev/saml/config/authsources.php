<?php

$config = array(

    'admin' => array(
        'core:AdminPassword',
    ),

    'example-userpass' => array(
        'exampleauth:UserPass',
        'rl:gocoyo' => array(
            'uid' => array('1'),
            'email' => 'robert.lang@coyo4.com',
        ),
        'nf:gocoyo' => array(
            'uid' => array('2'),
            'email' => 'nancy.fork@coyo4.com',
        ),
        'ib:gocoyo' => array(
            'uid' => array('3'),
            'email' => 'ian.bold@coyo4.com',
        ),
        'nf2:gocoyo' => array(
            'uid' => array('4'),
            'email' => 'nicholas.freeman@coyo4.com',
        ),
        'sm:gocoyo' => array(
            'uid' => array('5'),
            'email' => 'susanne.miller@coyo4.com',
        ),
        'mt:gocoyo' => array(
            'uid' => array('6'),
            'email' => 'michael.thomson@coyo4.com',
        ),
        'xyz:gocoyo' => array(
            'uid' => array('7'),
            'email' => 'unkwown.user@coyo4.com',
        )
    ),

);
