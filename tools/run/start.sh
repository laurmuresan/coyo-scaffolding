#!/bin/bash

set -e

echo "Starting monitoring stack ..."
docker-compose -f docker-compose.yml up -d --no-deps --remove-orphans \
    coyo-es-logs \
    coyo-logstash \
    coyo-logspout \
    coyo-kibana \
    coyo-telegraf \
    coyo-influxdb \
    coyo-grafana

echo "Starting dependencies ..."
docker-compose -f docker-compose.yml up -d --no-deps --remove-orphans \
    coyo-es \
    coyo-db \
    coyo-mq \
    coyo-tika \
    coyo-mongo \
    coyo-redis \
    coyo-scss \
    coyo-stomp

echo "Starting main stack ..."
docker-compose -f docker-compose.yml up -d --remove-orphans \
    coyo-backend \
    coyo-backup \
    coyo-frontend \
    coyo-push \
    coyo-docs

#start lb after all other containers to ensure haproxy get's the hosts
echo "Starting load balancer ..."
docker-compose -f docker-compose.yml up -d --remove-orphans --no-deps \
    coyo-lb

echo "Done!"
